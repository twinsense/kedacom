<form role="search" method="get" class="search-form" action="<?php echo home_url(); ?>">
    <label>
        <div class="toggle-search"><i class="fas fa-search"></i></div>
        <input class="search-field" placeholder="Search" value="" name="s" type="search">
    </label>
    <input class="search-submit" value="Search" type="submit">
</form>

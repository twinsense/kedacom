<?php if( have_rows('slides') ): ?>
    <div class="section header-product">

        <div class="container">
            <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                <?php if(function_exists('bcn_display')){
                    bcn_display();
                }?>
            </div>

            <div class="slider-holder">
                <div class="container">
                    <div id="header-slider-btn-left" class="slider-button btn-left"><?php echo file_get_contents(get_template_directory_uri()."/images/arrow_left.svg");?></div>
                    <div id="header-slider-btn-right" class="slider-button btn-right"><?php echo file_get_contents(get_template_directory_uri()."/images/arrow_right.svg");?></div>
                </div>
                <div class="header-slider">
                    <?php while( have_rows('slides') ): the_row(); ?>
                    
                        <?php $image = get_sub_field('afbeelding'); ?>
                        
                        <div class="header-slide">
                            <div class="row">
                                <div class="col-md-4 align-self-center">
                                    <img src="<?php echo $image['sizes']['header']; ?>" />
                                </div>
                                <div class="col-md-8 align-self-center">
                                    <?php if( have_rows('slide_inhoud') ): ?>
                                        <div class="text-holder">
                                            <?php while( have_rows('slide_inhoud') ): the_row(); ?>

                                                <?php if( get_row_layout() == 'titel' ): ?>
                                                    
                                                    <?php $h = get_sub_field('heading'); ?>
                                                    <<?php echo $h; ?>><?php the_sub_field('titel'); ?></<?php echo $h; ?>>

                                                <?php elseif( get_row_layout() == 'tekst' ): ?>

                                                    <?php the_sub_field('tekst'); ?>

                                                <?php elseif( get_row_layout() == 'knoppen' ): ?>

                                                    <?php if( have_rows('knoppen') ): ?>
                                                        <?php while ( have_rows('knoppen') ) : the_row(); ?>

                                                            <?php $style = get_sub_field('stijl');

                                                            $link = get_sub_field('link');
                                                                if( $link ):
                                                                    $link_url = $link['url'];
                                                                    $link_title = $link['title'];
                                                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                                                ?>
                                                                <a class="btn <?php echo $style; ?>" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                                                            <?php endif; ?>

                                                        <?php endwhile; ?>
                                                    <?php endif;?>
                                                    <div class="clearfix"></div>
                                                    
                                                <?php endif; ?>

                                            <?php endwhile; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
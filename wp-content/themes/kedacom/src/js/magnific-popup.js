// Import jQuery
import $ from 'jquery';

import 'magnific-popup';

// MagnificPopup configuration

$('.gallery-image').magnificPopup({
  type: 'image',
  gallery: {
    enabled: true
  }
});

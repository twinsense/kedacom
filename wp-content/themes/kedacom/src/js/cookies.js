// Import jQuery
import $ from 'jquery';

const Cookies = require('js-cookie');

// Cookie configuration

$(document).ready(function() {
  // Check if Cookies aren't set
  if (!Cookies.get('popupCookie')) {
    // If cookies aren't set add a delay to the popup
    setTimeout(function() {
      // If cookies aren't set then show the modal
      $('#modal-subscribe').modal('show');

      // Set the cookies for 7 days
      Cookies.set('popupCookie', true, {
        expires: 7
      });
    }, 2000);
  }
});

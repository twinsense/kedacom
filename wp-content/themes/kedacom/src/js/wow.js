// Import jQuery
import $ from 'jquery';

const WOW = require('wowjs');

// WOWjs
if (1025 <= $(window).width()) {
  window.wow = new WOW.WOW({
    live: false
  });

  window.wow.init({
    offset: 50
  });
}

import { library, dom } from '@fortawesome/fontawesome-svg-core';

import { faCheck } from '@fortawesome/free-solid-svg-icons/faCheck';
import { faMinus } from '@fortawesome/free-solid-svg-icons/faMinus';
import { faCar } from '@fortawesome/free-solid-svg-icons/faCar';
import { faSearch } from '@fortawesome/free-solid-svg-icons/faSearch';
import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes';
import { faBars } from '@fortawesome/free-solid-svg-icons/faBars';
import { faPhone } from '@fortawesome/free-solid-svg-icons/faPhone';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope';
import { faGlobe } from '@fortawesome/free-solid-svg-icons/faGlobe';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons/faFacebookF';
import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter';

library.add(
    faCheck, 
    faMinus, 
    faCar,
    faSearch,
    faTimes,
    faBars,
    faPhone,
    faEnvelope,
    faGlobe,
    faFacebookF,
    faTwitter
);

dom.watch();
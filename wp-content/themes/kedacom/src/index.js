// // Theme sass
import './sass/style.scss';

// Import own Javascript
import './js/cookies.js';
import './js/wow.js';
import './js/fontawesome.js';
import './js/magnific-popup.js';
import './js/matchheight.js';
import './js/other.js';

// Import modules

import 'animate.css';
import 'bootstrap/js/dist/util';
import 'bootstrap/js/dist/dropdown';
import 'bootstrap/js/dist/button';
import 'bootstrap/js/dist/collapse';
import 'bootstrap/js/dist/dropdown';
import 'bootstrap/js/dist/modal';
import 'bootstrap/js/dist/tab';
import 'bootstrap/js/dist/util.js';
